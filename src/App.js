import { Provider } from "react-redux";
import { BrowserRouter, Routes,Route } from "react-router-dom";
import "./App.css";
import Login from "./Components/Login";
import Translate from "./Components/Translate";



function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <header className="App-header">
          <h1>Translator</h1>
          <Routes>
            <Route path="/" element={<Login/>}/>
            <Route path="/translate" element={<Translate/>}/>
          </Routes>
        </header>
      </div>
    </BrowserRouter>
    
  );
}

export default App;
