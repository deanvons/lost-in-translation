import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

function Login() {
  const [registeredUsers, setRegisteredUsers] = useState([{ username: "" }]);
  const [user, setUser] = useState('');
  const navigate = useNavigate();

  useEffect(() => {
    getUsers();
  }, []);

  const apiURL = "https://noroff-assignment-users-api.herokuapp.com";
  const apiKey = "api_key";

  const getUsers = () => {
    const apiURL = "https://noroff-assignment-users-api.herokuapp.com";

    fetch(`${apiURL}/translations`)
      .then((response) => response.json())
      .then((results) => {
        setRegisteredUsers(results);
      })
      .catch((error) => {});
  };

  
  const registerUser = () => {
    fetch(`${apiURL}/translations`, {
      method: "POST",
      headers: {
        "X-API-Key": apiKey,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username: user,
        translations: [],
      }),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error("Could not create new user");
        }
        return response.json();
      })
      .then((newUser) => {
        setUser(newUser);
        //localStorage.setItem("userId", newUser.id);
        navigate("/translate");
      })
      .catch((error) => {});

      // set global state
  };

  const login = ()=>{
    navigate("/translate");

    // set global state
  }

  const handleOnChange = (event) => {
    setUser(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    let registered = false

    registeredUsers.forEach(registeredUser=>{
      if(registeredUser.username === user){
        registered = true
      }
    })


    if(registered){
      registerUser()
    }else{
      login()
    }

  };


  useEffect(() => {}, []);

  return (
    <form onSubmit={handleSubmit}>
      <h1></h1>
      <label>
        Name:
        <input type="text" name="name" onChange={handleOnChange} />
      </label>
      <button type="submit">Login</button>
    </form>
  );
}
export default Login;
